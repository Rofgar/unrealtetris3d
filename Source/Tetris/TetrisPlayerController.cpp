// Copyright Epic Games, Inc. All Rights Reserved.

#include "TetrisPlayerController.h"

ATetrisPlayerController::ATetrisPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
