// Copyright Epic Games, Inc. All Rights Reserved.

#include "TetrisBlock.h"
#include "TetrisBlockGrid.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Materials/MaterialInstanceDynamic.h"

ATetrisBlock::ATetrisBlock()
{
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> OrangeMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, OrangeMaterial(TEXT("/Game/Puzzle/Meshes/OrangeMaterial.OrangeMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(1.f,1.f,0.25f));
	BlockMesh->SetRelativeLocation(FVector(0.f,0.f,25.f));
	BlockMesh->SetMaterial(0, ConstructorStatics.BlueMaterial.Get());
	BlockMesh->SetupAttachment(DummyRoot);
	BlockMesh->OnClicked.AddDynamic(this, &ATetrisBlock::BlockClicked);
	BlockMesh->OnInputTouchBegin.AddDynamic(this, &ATetrisBlock::OnFingerPressedBlock);

	// Save a pointer to the orange material
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	OrangeMaterial = ConstructorStatics.OrangeMaterial.Get();
}

void ATetrisBlock::BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	HandleClicked();
}


void ATetrisBlock::OnFingerPressedBlock(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
	HandleClicked();
}

void ATetrisBlock::HandleClicked()
{
	// Check we are not already active
	if (!bIsActive)
	{
		bIsActive = true;

		// Change material
		BlockMesh->SetMaterial(0, OrangeMaterial);

		// Tell the Grid
		if (OwningGrid != nullptr)
		{
			OwningGrid->AddScore();
		}
	}
}


void ATetrisBlock::Highlight(bool bOn)
{
	// Do not highlight if the block has already been activated.
	if (bIsActive)
	{
		return;
	}

	if (bOn)
	{
		auto dynamicMaterialInstance = BlockMesh->CreateDynamicMaterialInstance(0, BaseMaterial);

		auto r = std::rand() % 255;
		auto g = std::rand() % 255;
		auto b = std::rand() % 255;

		dynamicMaterialInstance->SetVectorParameterValue("DiffuseColor", FColor(r, g, b));
	}
	else
	{
		BlockMesh->SetMaterial(0, BlueMaterial);
	}
}

void ATetrisBlock::MoveDownOneSize() {
	float blockHeight = GetBlockMesh()->GetStaticMesh()->GetBoundingBox().GetSize().Z;
	float spacingDiff = OwningGrid->BlockSpacing - blockHeight;
	this->AddActorLocalOffset(-FVector(0.f, 0.f, blockHeight + spacingDiff));
}

void ATetrisBlock::SetCellIndex(uint32 newZIndex, uint32 newYIndex) {
	zIndex = newZIndex;
	yIndex = newYIndex;
}

bool ATetrisBlock::isStillMoving()
{
	return StillMoving;
}

void ATetrisBlock::MoveLeft() {
	float blockHeight = GetBlockMesh()->GetStaticMesh()->GetBoundingBox().GetSize().Y;
	float spacingDiff = OwningGrid->BlockSpacing - blockHeight;
	this->AddActorLocalOffset(-FVector(0.f, blockHeight + spacingDiff, 0.f));
}

void ATetrisBlock::MoveRight() {
	float blockHeight = GetBlockMesh()->GetStaticMesh()->GetBoundingBox().GetSize().Y;
	float spacingDiff = OwningGrid->BlockSpacing - blockHeight;
	this->AddActorLocalOffset(FVector(0.f, blockHeight + spacingDiff, 0.f));
}