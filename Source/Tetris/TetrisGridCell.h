#pragma once
#include "CoreMinimal.h"
#include "TetrisBlock.h"

class TetrisGridCell {


public:
	ATetrisBlock* Occupant;
	FVector BlockLocation;

public:
	TetrisGridCell(FVector BlockLocation);
	bool isOccupied();
};