#include "TetrisGridCell.h"

TetrisGridCell::TetrisGridCell(FVector BlockLocation) {
	this->Occupant = nullptr;
	this->BlockLocation = BlockLocation;
}

bool TetrisGridCell::isOccupied() {
	return this->Occupant != nullptr;
}