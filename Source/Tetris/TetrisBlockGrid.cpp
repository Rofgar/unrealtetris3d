// Copyright Epic Games, Inc. All Rights Reserved.

#include "TetrisBlockGrid.h"
#include "TetrisBlock.h"
#include "Components/TextRenderComponent.h"
#include "Engine/World.h"
#include "TetrisGridCell.h"

#define LOCTEXT_NAMESPACE "PuzzleBlockGrid"

ATetrisBlockGrid::ATetrisBlockGrid()
{
	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	PrimaryActorTick.bCanEverTick = true;

	// Set defaults
	Height = 8;
	Width = 3;
	BlockSpacing = 300.f;

	LastMoveTime = 0.f;

	DropRate = 1.f;

	// Create static mesh component
	ScoreText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("ScoreText0"));
	ScoreText->SetRelativeLocation(FVector(200.f,0.f,0.f));
	ScoreText->SetRelativeRotation(FRotator(0.f,0.f,0.f));
	ScoreText->SetText(FText::Format(LOCTEXT("ScoreFmt", "Score: {0}"), FText::AsNumber(0)));
	ScoreText->SetupAttachment(DummyRoot);
	ScoreText->SetRelativeScale3D(this->GetActorRelativeScale3D());
}



void ATetrisBlockGrid::MoveBlockLeft()
{
	if (ActiveBlock != nullptr && ActiveBlock->yIndex > 0) {
		ActiveBlock->MoveLeft();
		TetrisGridCell* CurrentCell = &CellMap[ActiveBlock->zIndex][ActiveBlock->yIndex];
		TetrisGridCell* LeftCell = &CellMap[ActiveBlock->zIndex][ActiveBlock->yIndex - 1];
		if (!LeftCell->isOccupied()) {
			LeftCell->Occupant = ActiveBlock;
			CurrentCell->Occupant = nullptr;
			ActiveBlock->yIndex -= 1;
		}
	}
}

void ATetrisBlockGrid::MoveBlockRight()
{
	if (ActiveBlock != nullptr && ActiveBlock->yIndex < (uint32) Width - 1) {
		ActiveBlock->MoveRight();
		TetrisGridCell* CurrentCell = &CellMap[ActiveBlock->zIndex][ActiveBlock->yIndex];
		TetrisGridCell* RightCell = &CellMap[ActiveBlock->zIndex][ActiveBlock->yIndex + 1];
		if (!RightCell->isOccupied()) {
			RightCell->Occupant = ActiveBlock;
			CurrentCell->Occupant = nullptr;
			ActiveBlock->yIndex += 1;
		}
	}
}

void ATetrisBlockGrid::BeginPlay()
{
	Super::BeginPlay();

	InputComponent->BindAction("MoveBlockLeft", EInputEvent::IE_Pressed, this, &ATetrisBlockGrid::MoveBlockLeft);
	InputComponent->BindAction("MoveBlockRight", EInputEvent::IE_Pressed, this, &ATetrisBlockGrid::MoveBlockRight);

	// Loop to spawn each block
	for (int32 RowIndex = 0; RowIndex < Height; RowIndex++) {
		std::vector<TetrisGridCell> ColumnCells;
		for (int32 ColumnIndex = 0; ColumnIndex < Width; ColumnIndex++)
		{
			const float ZOffset = RowIndex * BlockSpacing * this->GetActorRelativeScale3D().Z;
			const float YOffset = ColumnIndex * BlockSpacing * this->GetActorRelativeScale3D().Y;

			// Make position vector, offset from Grid location
			const FVector BlockLocation = FVector(0.f, YOffset, ZOffset) + GetActorLocation();

			ColumnCells.push_back(TetrisGridCell(BlockLocation));
		}
		CellMap.push_back(ColumnCells);
	}

	SpawnShapeAtTop();
}

void ATetrisBlockGrid::SpawnShapeAtTop() {
	uint32 zIndex = Height - 1;
	uint32 yIndex = 2 % Width;

	TetrisGridCell* SpawnCell = &CellMap[zIndex][yIndex];
	if (SpawnCell->isOccupied()) {
		ScoreText->SetText(FText::Format(LOCTEXT("ScoreFmt", "Finished with Score: {0}"), FText::AsNumber(Score)));
		ActiveBlock = nullptr;
		return;
	}

	if (SpawnCell->Occupant == nullptr) {
		ATetrisBlock* NewBlock = GetWorld()->SpawnActor<ATetrisBlock>(CellMap[zIndex][yIndex].BlockLocation, FRotator(0, 0, 0));

		// Tell the block about its owner
		if (NewBlock != nullptr)
		{
			NewBlock->GetBlockMesh()->SetRelativeScale3D(this->GetActorRelativeScale3D());
			NewBlock->OwningGrid = this;
			NewBlock->StillMoving = true;
			NewBlock->SetCellIndex(zIndex, yIndex);
			CellMap[NewBlock->zIndex][NewBlock->yIndex].Occupant = NewBlock;
			ActiveBlock = NewBlock;
		}
	}
}

void ATetrisBlockGrid::Tick(float DeltaSeconds)
{
	LastMoveTime += DeltaSeconds;
	if (LastMoveTime > DropRate) {
		if (ActiveBlock == nullptr) {
			SpawnShapeAtTop();
			LastMoveTime = 0.f;
		}
		else {
			if (ActiveBlock->zIndex > 0) {
				TetrisGridCell* CurrentCell = &CellMap[ActiveBlock->zIndex][ActiveBlock->yIndex];
				TetrisGridCell* LowerCell = &CellMap[ActiveBlock->zIndex - 1][ActiveBlock->yIndex];
				if (LowerCell->Occupant == nullptr) {
					ActiveBlock->MoveDownOneSize();
					LowerCell->Occupant = ActiveBlock;
					CurrentCell->Occupant = nullptr;
					ActiveBlock->zIndex -= 1;
				}
				else {
					ActiveBlock->StillMoving = false;
					SpawnShapeAtTop();
				}
			}
			else {
				ActiveBlock->StillMoving = false;
				SpawnShapeAtTop();
			}
			LastMoveTime = 0.f;
		}
		ProcessScoreRows();
	}
}

void ATetrisBlockGrid::ProcessScoreRows() {
	uint32 CurrentRowIndex = 0;
	std::vector<uint32> RemovedRows;
	for (std::vector<TetrisGridCell> &Row : CellMap) {
		bool FoundUnOccupied = false;
		for (TetrisGridCell &Cell : Row) {
			if (Cell.Occupant == nullptr) {
				FoundUnOccupied = true;
				break;
			}
		}

		bool FoundStillMoving = false;
		for (TetrisGridCell& Cell : Row) {
			if (Cell.Occupant != nullptr && Cell.Occupant->isStillMoving()) {
				FoundStillMoving = true;
				break;
			}
		}
		
		if (!FoundUnOccupied && !FoundStillMoving) {
			RemoveRow(Row);
			RemovedRows.push_back(CurrentRowIndex);
			Score += 100;
			ScoreText->SetText(FText::Format(LOCTEXT("ScoreFmt", "Score: {0}"), FText::AsNumber(Score)));
		}
		CurrentRowIndex += 1;
	}

	for (uint32 RemovedRowIndex : RemovedRows) {
		uint32 MovingRowIndex = RemovedRowIndex;
		for (uint32 NextRowIndex = MovingRowIndex + 1; NextRowIndex < CellMap.size(); ++NextRowIndex) {
			std::vector<TetrisGridCell>& SourceRow = CellMap[NextRowIndex];
			std::vector<TetrisGridCell>& TargetRow = CellMap[MovingRowIndex];
			SwapUpDownRows(SourceRow, TargetRow);
			MovingRowIndex += 1;
		}
	}

}

void ATetrisBlockGrid::RemoveRow(std::vector<TetrisGridCell>& Row) {
	for (auto& Cell : Row) {
		if (ActiveBlock == Cell.Occupant) {
			ActiveBlock = nullptr;
		}
		ATetrisBlock* Occupant = Cell.Occupant;
		Occupant->Destroy();
		Cell.Occupant = nullptr;
	}
}

void ATetrisBlockGrid::SwapUpDownRows(std::vector<TetrisGridCell>& SourceRow, std::vector<TetrisGridCell>& TargetRow) {
	for (uint32 i = 0; i < SourceRow.size(); ++i) {
		TetrisGridCell& SourceCell = SourceRow[i];
		if (SourceCell.isOccupied()) {
			TetrisGridCell& TargetCell = TargetRow[i];
			if (!TargetCell.isOccupied() && SourceCell.Occupant != ActiveBlock) {
				SourceCell.Occupant->MoveDownOneSize();
				TargetCell.Occupant = SourceCell.Occupant;
				SourceCell.Occupant = nullptr;
			}
		}
	}
}

void ATetrisBlockGrid::AddScore()
{
	// Increment score
	Score++;

	// Update text
	ScoreText->SetText(FText::Format(LOCTEXT("ScoreFmt", "Score: {0}"), FText::AsNumber(Score)));
}

#undef LOCTEXT_NAMESPACE
