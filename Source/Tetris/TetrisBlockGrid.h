// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "TetrisGridCell.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TetrisBlockGrid.generated.h"

/** Class used to spawn blocks and manage score */
UCLASS(minimalapi)
class ATetrisBlockGrid : public AActor
{
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	/** Text component for the score */
	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTextRenderComponent* ScoreText;

	std::vector<std::vector<TetrisGridCell>> CellMap;

	class ATetrisBlock* ActiveBlock;

	float LastMoveTime;

public:
	ATetrisBlockGrid();

	/** How many blocks have been clicked */
	int32 Score;

	/** Number of blocks along each side of grid */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 Height;

	/** Number of blocks along each side of grid */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 Width;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 BlockSpawnHeight;

	/** Spacing of blocks */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	float BlockSpacing;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	float DropRate;

protected:
	// Begin AActor interface
	virtual void BeginPlay() override;
	
	// End AActor interface

public:
	virtual void Tick(float DeltaSeconds) override;

	/** Handle the block being clicked */
	void AddScore();
	
	void SpawnShapeAtTop();

	void ProcessScoreRows();

	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns ScoreText subobject **/
	FORCEINLINE class UTextRenderComponent* GetScoreText() const { return ScoreText; }

	void RemoveRow(std::vector<TetrisGridCell>& Row);

	void SwapUpDownRows(std::vector<TetrisGridCell>& SuourceRow, std::vector<TetrisGridCell>& TargetRow);

	void MoveBlockLeft();

	void MoveBlockRight();

};



